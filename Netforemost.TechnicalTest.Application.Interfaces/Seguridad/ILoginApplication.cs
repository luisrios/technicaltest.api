﻿using Netforemost.TechnicalTest.Dto;
using Netforemost.TechnicalTest.Model.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Application.Interfaces.Seguridad
{
    public interface ILoginApplication
    {
        Task<ResponseDTO> Login(LoginModel model);
    }
}
