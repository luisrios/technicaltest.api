﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Dto.Seguridad
{
    public class TokenDTO
    {
        public string Value { get; set; }
        public DateTime? FechaExpiracion { get; set; }
    }
}
