﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Dto.Seguridad
{
    public class UsuarioDTO
    {
        public int UsuarioId { get; set; }
        public string NombresApellidos { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public string Codigo { get; set; }

    }
}
