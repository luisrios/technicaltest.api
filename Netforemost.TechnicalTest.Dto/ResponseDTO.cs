﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Netforemost.TechnicalTest.Common.Constants;
using static Netforemost.TechnicalTest.Common.Constants.Common;

namespace Netforemost.TechnicalTest.Dto
{
    public class ResponseDTO
    {
        public ResponseDTO()
        {
            this.Status = EstadoRespuesta.OK;
            this.TransactionId = DateTime.Now.ToString(Core.DateTimeFormats.DD_MM_YYYY_HH_MM_SS_FFF);
        }

        public string TransactionId { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }
    }

    public class ResponseDTO<T>
    {
        public ResponseDTO()
        {
            this.Status = EstadoRespuesta.OK;
            this.TransactionId = DateTime.Now.ToString(Core.DateTimeFormats.DD_MM_YYYY_HH_MM_SS_FFF);
        }

        public string TransactionId { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }

    }
}
