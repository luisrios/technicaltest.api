﻿using Microsoft.EntityFrameworkCore;
using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using Netforemost.TechnicalTest.Dto.Seguridad;
using Netforemost.TechnicalTest.Model.Seguridad;
using Netforemost.TechnicalTest.Repository.Implementations.Data;
using Netforemost.TechnicalTest.Repository.Implementations.Data.Base;
using Netforemost.TechnicalTest.Repository.Interfaces.Repositories.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Implementations.Repositories.Seguridad
{
    internal class UsuarioRepository : BaseRepository<UsuarioEntity>, IUsuarioRepository
    {
        private readonly DataContext context;

        public UsuarioRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public async Task<UsuarioDTO> ObtenerUsuarioPorCorreoYClave(LoginModel model)
        {
            return await context.Usuario.AsQueryable().Where(p => p.RowStatus && p.Usuario.ToLower() == model.Correo.Trim().ToLower())
                        .Select(s => new UsuarioDTO()
                        {
                            UsuarioId = s.Id,
                            Codigo = s.Usuario,
                            Nombres = s.Persona != null ? s.Persona.Nombres : string.Empty,
                            Correo = s.Persona != null ? s.Persona.Correo : string.Empty,
                            Apellidos = s.Persona != null ? s.Persona.Apellidos : string.Empty
                        }).FirstOrDefaultAsync();
        }

        public async Task<List<UsuarioEntity>> ObtenerUsuariosEntityPorIds(List<int> ids)
        {
            return await context.Usuario
                .Include(x => x.Persona)
                .Include(x => x.HistorialBusquedaUsuario)
                .Where(x => x.RowStatus && ids.Contains(x.Id))
                .ToListAsync();
        }

        public async Task<List<UsuarioDTO>> ObtenerUsuariosNuevos()
        {
            return await context.Usuario.AsQueryable().Where(p => p.RowStatus && !p.FlagEnvioCorreoBienvenida)
                        .Select(s => new UsuarioDTO()
                        {
                            UsuarioId = s.Id,
                            Codigo = s.Usuario,
                            Nombres = s.Persona != null ? s.Persona.Nombres : string.Empty,
                            Correo = s.Persona != null ? s.Persona.Correo : string.Empty,
                            Apellidos = s.Persona != null ? s.Persona.Apellidos : string.Empty
                        }).ToListAsync();
        }
    }
}
