﻿using Microsoft.EntityFrameworkCore;
using Netforemost.TechnicalTest.Domain.Models.Entities.HistorialBusqueda;
using Netforemost.TechnicalTest.Domain.Models.Entities.Persona;
using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using Netforemost.TechnicalTest.Repository.Implementations.Configurations.HistorialBusqueda;
using Netforemost.TechnicalTest.Repository.Implementations.Configurations.Persona;
using Netforemost.TechnicalTest.Repository.Implementations.Configurations.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Implementations.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UsuarioConfiguration(builder));
            builder.ApplyConfiguration(new PersonaConfiguration(builder));
            builder.ApplyConfiguration(new HistorialBusquedaUsuarioConfiguration(builder));
        }

        public DbSet<UsuarioEntity> Usuario { get; set; }
        public DbSet<PersonaEntity> Persona { get; set; }
        public DbSet<HistorialBusquedaUsuarioEntity> HistorialBusquedaUsuario { get; set; }
    }
}
