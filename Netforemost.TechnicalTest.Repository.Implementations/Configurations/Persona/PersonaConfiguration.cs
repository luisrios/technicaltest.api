﻿using Microsoft.EntityFrameworkCore;
using Netforemost.TechnicalTest.Domain.Models.Entities.Persona;
using Netforemost.TechnicalTest.Repository.Implementations.Configurations.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Implementations.Configurations.Persona
{
    public class PersonaConfiguration : EntityConfiguration<PersonaEntity>
    {
        public PersonaConfiguration(ModelBuilder builder)
        {
            var entityBuilder = builder.Entity<PersonaEntity>();
            entityBuilder.ToTable("TechnicalTest_Persona");
            entityBuilder.HasKey(c => c.Id);
            entityBuilder.Property(c => c.Nombres).HasColumnName("VC_NOMBRES");
            entityBuilder.Property(c => c.Apellidos).HasColumnName("VC_APELLIDOS");
            entityBuilder.Property(c => c.Correo).HasColumnName("VC_CORREO");

            Configure(entityBuilder);
        }
    }
}
