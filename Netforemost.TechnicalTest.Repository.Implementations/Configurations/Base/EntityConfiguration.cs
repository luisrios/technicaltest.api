﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Netforemost.TechnicalTest.Repository.Interfaces.Configurations.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Implementations.Configurations.Base
{
    public abstract class EntityConfiguration<T> : IEntityConfiguration<T> where T : class
    {
        public void Configure(EntityTypeBuilder<T> builder)
        {
            builder.Property("Id").HasColumnName("IN_ID").UseIdentityColumn();
            builder.Property("CreationUser").HasColumnName("VC_USUARIO_CREACION");
            builder.Property("CreationDate").HasColumnName("DT_FECHA_CREACION");
            builder.Property("ModificationUser").HasColumnName("VC_USUARIO_MODIFICACION");
            builder.Property("ModificationDate").HasColumnName("DT_FECHA_MODIFICACION");
            builder.Property("RowStatus").HasColumnName("BT_ESTADO_FILA");
        }
    }
}
