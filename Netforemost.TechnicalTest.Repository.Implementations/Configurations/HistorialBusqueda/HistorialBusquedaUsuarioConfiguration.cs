﻿using Microsoft.EntityFrameworkCore;
using Netforemost.TechnicalTest.Domain.Models.Entities.HistorialBusqueda;
using Netforemost.TechnicalTest.Repository.Implementations.Configurations.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Implementations.Configurations.HistorialBusqueda
{
    public class HistorialBusquedaUsuarioConfiguration : EntityConfiguration<HistorialBusquedaUsuarioEntity>
    {
        public HistorialBusquedaUsuarioConfiguration(ModelBuilder builder)
        {
            var entityBuilder = builder.Entity<HistorialBusquedaUsuarioEntity>();
            entityBuilder.ToTable("TechnicalTest_Historial_Busqueda_Usuario");
            entityBuilder.HasKey(c => c.Id);
            entityBuilder.Property(c => c.Busqueda).HasColumnName("VC_BUSQUEDA");
            entityBuilder.Property(c => c.UsuarioId).HasColumnName("IN_USUARIO_ID");

            entityBuilder.HasOne(d => d.Usuario).WithMany(p => p.HistorialBusquedaUsuario).HasForeignKey(d => d.UsuarioId);

            Configure(entityBuilder);
        }
    }
}
