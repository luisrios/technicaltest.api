﻿using Microsoft.EntityFrameworkCore;
using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using Netforemost.TechnicalTest.Repository.Implementations.Configurations.Base;
using Netforemost.TechnicalTest.Repository.Interfaces.Configurations.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Implementations.Configurations.Usuario
{
    public class UsuarioConfiguration : EntityConfiguration<UsuarioEntity>
    {
        public UsuarioConfiguration(ModelBuilder builder)
        {
            var entityBuilder = builder.Entity<UsuarioEntity>();
            entityBuilder.ToTable("TechnicalTest_Usuario");
            entityBuilder.HasKey(c => c.Id);
            entityBuilder.Property(c => c.Usuario).HasColumnName("VC_USUARIO");
            entityBuilder.Property(c => c.Contrasena).HasColumnName("VC_CONTRASENA");
            entityBuilder.Property(c => c.PersonaId).HasColumnName("IN_PERSONA_ID");
            entityBuilder.Property(c => c.FlagEnvioCorreoBienvenida).HasColumnName("BT_ENVIO_CORREO_BIENVENIDA");
            entityBuilder.HasOne(d => d.Persona).WithMany(p => p.Usuarios).HasForeignKey(d => d.PersonaId);

            Configure(entityBuilder);
        }
    }
}
