﻿using Netforemost.TechnicalTest.Domain.Models.Common;
using Netforemost.TechnicalTest.Domain.Models.Entities.HistorialBusqueda;
using Netforemost.TechnicalTest.Domain.Models.Entities.Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Domain.Models.Entities.Usuario
{
    public class UsuarioEntity : Entity
    {
        public string? Usuario { get; set; }
        public string? Contrasena { get; set; }
        public int? PersonaId { get; set; }
        public bool FlagEnvioCorreoBienvenida { get; set; }
        public virtual PersonaEntity? Persona { get; set; }
        public virtual ICollection<HistorialBusquedaUsuarioEntity>? HistorialBusquedaUsuario { get; set; }
    }
}
