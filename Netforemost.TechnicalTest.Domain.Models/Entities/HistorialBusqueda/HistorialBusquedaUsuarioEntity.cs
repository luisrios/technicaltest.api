﻿using Netforemost.TechnicalTest.Domain.Models.Common;
using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Domain.Models.Entities.HistorialBusqueda
{
    public class HistorialBusquedaUsuarioEntity : Entity
    {
        public string? Busqueda { get; set; }
        public int? UsuarioId { get; set; }
        public virtual UsuarioEntity? Usuario { get; set; }
    }
}
