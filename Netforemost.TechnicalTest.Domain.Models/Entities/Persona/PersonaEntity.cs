﻿using Netforemost.TechnicalTest.Domain.Models.Common;
using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Domain.Models.Entities.Persona
{
    public class PersonaEntity : Entity
    {
        public string? Nombres { get; set; }
        public string? Apellidos { get; set; }
        public string? Correo { get; set; }
        public virtual ICollection<UsuarioEntity>? Usuarios { get; set; }
    }
}
