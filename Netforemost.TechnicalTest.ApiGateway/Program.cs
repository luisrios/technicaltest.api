﻿
using Autofac.Extensions.DependencyInjection;

namespace Netforemost.TechnicalTest.ApiGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var conf = GetConfig();

            IWebHostBuilder builder = new WebHostBuilder();
            builder.ConfigureServices(s =>
            {
                s.AddSingleton(builder);
            });
            builder.UseKestrel(options =>
            {
                options.Limits.MaxRequestBodySize = long.MaxValue;
            })
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseStartup<Startup>()
            .ConfigureServices(service => service.AddAutofac())
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.AddEnvironmentVariables();
            })
            .UseUrls($"http://*:{conf.GetSection("Host:Port").Get<string>()}");
            var host = builder.Build();
            host.Run();
        }

        private static IConfigurationRoot GetConfig()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            string appSettingName = string.Empty;
            if (env == "Production")
                appSettingName = ".Production";

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings{appSettingName}.json", optional: true)
                .AddEnvironmentVariables();

            return builder.Build(); ;
        }
    }

}
