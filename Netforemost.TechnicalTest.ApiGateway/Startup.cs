﻿using Autofac;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Netforemost.TechnicalTest.Common.Configurations;
using Netforemost.TechnicalTest.Common.Scheme;
using Netforemost.TechnicalTest.Repository.Implementations.Data.Base;
using Netforemost.TechnicalTest.Repository.Implementations.Data;
using Netforemost.TechnicalTest.Repository.Interfaces.Data;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System.Reflection;
using System.Text;
using Netforemost.TechnicalTest.Extensions;

namespace Netforemost.TechnicalTest.ApiGateway
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostEnvironment env)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            string appSettingName = string.Empty;
            if (environmentName == "Production")
                appSettingName = ".Production";

            var builder = new ConfigurationBuilder();
            builder.SetBasePath(env.ContentRootPath)
               .AddJsonFile($"appsettings{appSettingName}.json", optional: true, reloadOnChange: true)
               .AddJsonFile($"ocelot{appSettingName}.json", optional: false, reloadOnChange: true)
               .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = services.AddAppSettingExtension(Configuration);

            var appSettings = appSettingsSection.Get<Common.Configurations.AppSettings>();


            services.Configure<AppSettings>(appSettingsSection);
            services.AddSingleton(cfg => cfg.GetService<IOptions<AppSettings>>().Value);
            services.AddTransient(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddMvc();


            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(appSettings.JWTokenConfiguration.Secret));

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                ValidateIssuer = true,
                ValidIssuer = appSettings.JWTokenConfiguration.Iss,
                ValidateAudience = true,
                ValidAudience = appSettings.JWTokenConfiguration.Aud,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true,
            };


            services.AddAuthentication()
             .AddJwtBearer(AuthenticateScheme.Seguridad, x =>
             {
                 x.RequireHttpsMetadata = false;
                 x.TokenValidationParameters = tokenValidationParameters;
             });

            services.AddCors();
            services.AddOcelot(Configuration);
            services.AddSwaggerForOcelot(Configuration);
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "TechnicalTest API",
                        Version = "v1"
                    });
            });
            services.AddSwaggerGen(c =>
                c.MapType<TimeSpan?>(() =>
                new OpenApiSchema
                {
                    Type = "string",
                    Example = new OpenApiString("00:00:00")
                }));
        }

        public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider, IWebHostEnvironment env)
        {
            string pathBase = Configuration.GetSection("Host:PathBase").Get<string>();


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(x => x
             .AllowAnyOrigin()
             .AllowAnyMethod()
             .AllowAnyHeader());

            app.UseRouting();
            app.UseAuthentication();
            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers(); ;
            });



            app.UseHsts();
            app.UseCookiePolicy(new CookiePolicyOptions() { Secure = CookieSecurePolicy.Always });
            app.UseSwagger();

            app.UseSwaggerForOcelotUI(c =>
            {
                c.DownstreamSwaggerEndPointBasePath = $"{pathBase}/swagger/docs";
                c.PathToSwaggerGenerator = $"{pathBase}/swagger/docs";
            });

            app.Use((context, next) =>
            {
                context.Request.PathBase = new PathString(pathBase);
                return next();
            });

            app.UseOcelot();
        }
        public void ConfigureContainer(ContainerBuilder builder)
        {
            var assemblies = new List<Assembly>();
            var dependencies = DependencyContext.Default.RuntimeLibraries;
            foreach (var library in dependencies)
            {
                if (library.Name.StartsWith("Netforemost"))
                {
                    var assembly = Assembly.Load(new AssemblyName(library.Name));
                    assemblies.Add(assembly);
                }
            }

            var assembliesArray = assemblies.ToArray();
            builder.RegisterAssemblyTypes(assembliesArray).Where(t => t.Name.EndsWith("Service")).AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterAssemblyTypes(assembliesArray).Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterAssemblyTypes(assembliesArray).Where(t => t.Name.EndsWith("Application")).AsImplementedInterfaces().InstancePerDependency();

            Autofac.IContainer container = null;
            builder.Register(c => container).AsSelf().SingleInstance();

            builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IBaseRepository<>)).InstancePerDependency();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().SingleInstance();
        }
    }

}
