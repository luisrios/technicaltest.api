﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Model.Seguridad
{
    public class LoginModel
    {
        public string? Correo { get; set; }
        public string? Clave { get; set; }
    }
}
