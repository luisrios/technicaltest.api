﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Netforemost.TechnicalTest.Common.Configurations;

namespace Netforemost.TechnicalTest.Extensions
{
    public static class AppSettingExtension
    {
        public static IConfigurationSection AddAppSettingExtension(this IServiceCollection services, IConfiguration configuration)
        {
            var appSettingsSection = configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            services.AddSingleton(cfg => cfg.GetService<IOptions<AppSettings>>().Value);

            return appSettingsSection;
        }
    }
}