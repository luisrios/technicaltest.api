﻿CREATE TABLE TechnicalTest_Persona(
IN_ID int identity(1,1) primary key not null,
VC_NOMBRES varchar(100) null,
VC_APELLIDOS VARCHAR(100) null,
VC_CORREO VARCHAR(100) null,
VC_USUARIO_CREACION varchar(100) null,
DT_FECHA_CREACION datetime,
VC_USUARIO_MODIFICACION varchar(100) null,
DT_FECHA_MODIFICACION datetime null,
BT_ESTADO_FILA bit
);

CREATE TABLE TechnicalTest_Usuario(
IN_ID int identity(1,1) primary key not null,
VC_USUARIO varchar(100) null,
VC_CONTRASENA VARCHAR(100) null,
IN_PERSONA_ID INT NULL,
VC_USUARIO_CREACION varchar(100) null,
BT_ENVIO_CORREO_BIENVENIDA bit default 0,
DT_FECHA_CREACION datetime,
VC_USUARIO_MODIFICACION varchar(100) null,
DT_FECHA_MODIFICACION datetime null,
BT_ESTADO_FILA bit
);

ALTER TABLE TechnicalTest_Usuario
ADD FOREIGN KEY (IN_PERSONA_ID) REFERENCES TechnicalTest_Persona(IN_ID);


CREATE TABLE TechnicalTest_Historial_Busqueda_Usuario(
IN_ID int identity(1,1) primary key not null,
IN_USUARIO_ID INT NULL,
VC_BUSQUEDA varchar(100) null,
VC_USUARIO_CREACION varchar(100) null,
DT_FECHA_CREACION datetime,
VC_USUARIO_MODIFICACION varchar(100) null,
DT_FECHA_MODIFICACION datetime null,
BT_ESTADO_FILA bit
)

ALTER TABLE TechnicalTest_Historial_Busqueda_Usuario
ADD FOREIGN KEY (IN_USUARIO_ID) REFERENCES TechnicalTest_Usuario(IN_ID);