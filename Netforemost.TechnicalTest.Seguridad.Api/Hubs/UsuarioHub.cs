﻿using Microsoft.AspNetCore.SignalR;

namespace Netforemost.TechnicalTest.Seguridad.Api.Hubs
{
    public class UsuarioHub : Hub
    {
        public async Task SendMessage(string user)
        {
            await Clients.All.SendAsync("ReceiveMessage", user);
        }
    }
}
