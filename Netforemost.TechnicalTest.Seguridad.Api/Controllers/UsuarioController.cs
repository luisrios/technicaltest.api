﻿using Autofac;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Netforemost.TechnicalTest.Application.Implementations.Seguridad;
using Netforemost.TechnicalTest.Application.Interfaces.Seguridad;
using Netforemost.TechnicalTest.Common;
using Netforemost.TechnicalTest.Common.Exceptions;
using Netforemost.TechnicalTest.Common.Resources;
using Netforemost.TechnicalTest.Common.Scheme;
using Netforemost.TechnicalTest.Dto;
using Netforemost.TechnicalTest.Dto.Seguridad;
using Netforemost.TechnicalTest.Model.Seguridad;
using Netforemost.TechnicalTest.Seguridad.Api.Hubs;
using Netforemost.TechnicalTest.Seguridad.Api.Validators.Usuario;
using static Netforemost.TechnicalTest.Common.Constants.Common;

namespace Netforemost.TechnicalTest.Seguridad.Api.Controllers
{
    [Authorize]
    [Authorize(AuthenticationSchemes = AuthenticateScheme.Seguridad)]
    [Route("[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly ILogger<UsuarioController> _logger;
        private readonly Lazy<IUsuarioApplication> _usuarioApplication;
        private readonly Lazy<IHubContext<UsuarioHub>> _hubContext;

        public UsuarioController(ILogger<UsuarioController> logger, ILifetimeScope lifetimeScope)
        {
            _logger = logger;
            _usuarioApplication = new Lazy<IUsuarioApplication>(() => lifetimeScope.Resolve<IUsuarioApplication>());
            _hubContext = new Lazy<IHubContext<UsuarioHub>>(() => lifetimeScope.Resolve<IHubContext<UsuarioHub>>());
        }
        private IUsuarioApplication UsuarioApplication => _usuarioApplication.Value;
        private IHubContext<UsuarioHub> HubContext => _hubContext.Value;

        [AllowAnonymous]
        [HttpPost]
        [Route("CrearUsuario")]
        public async Task<IActionResult> CrearUsuario([FromBody] CrearUsuarioModel model)
        {
            ResponseDTO response;

            try
            {
                if (model == null)
                    throw new FunctionalException(CommonResource.campo_requerido, "model = null");

                var servicioValidator = new CrearUsuarioValidator().Validate(model);

                if (servicioValidator != null)
                {
                    if (!servicioValidator.IsValid)
                        throw new FunctionalException(CommonResource.campo_requerido, servicioValidator.ToString());
                }
                else
                    throw new FunctionalException(CommonResource.campo_requerido, "validator = null");


                response = await UsuarioApplication.CrearUsuario(model);

                if (response.Status == Constants.Common.EstadoRespuesta.OK)
                {
                    var usuarioDto = (UsuarioDTO)response.Data;
                    await HubContext.Clients.All.SendAsync("ReceiveMessage", usuarioDto.NombresApellidos);
                }
            }
            catch (FunctionalException ex)
            {
                response = new ResponseDTO { Status = ex.Status, Message = ex.Message, Data = ex.Data, TransactionId = ex.TransactionId };
                _logger.LogWarning(ex.TransactionId, ex.Message, ex);
            }
            catch (TechnicalException ex)
            {
                response = new ResponseDTO { Status = ex.Status, Message = ex.Message, Data = ex.Data, TransactionId = ex.TransactionId };
                _logger.LogError(ex.TransactionId, ex.Message, ex);
            }
            catch (Exception ex)
            {
                response = new ResponseDTO { Status = EstadoRespuesta.ERROR_TECNICO, Message = ex.Message };
                _logger.LogError(response.TransactionId, ex.Message, ex);
            }
            return Ok(response);
        }
    }
}
