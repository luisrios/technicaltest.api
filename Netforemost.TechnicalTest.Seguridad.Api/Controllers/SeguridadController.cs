using Autofac;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Netforemost.TechnicalTest.Application.Interfaces.Seguridad;
using Netforemost.TechnicalTest.Common.Exceptions;
using Netforemost.TechnicalTest.Common.Resources;
using Netforemost.TechnicalTest.Common.Scheme;
using Netforemost.TechnicalTest.Dto;
using Netforemost.TechnicalTest.Model.Seguridad;
using Netforemost.TechnicalTest.Seguridad.Api.Validators.Seguridad;
using Netforemost.TechnicalTest.Seguridad.Api.Validators.Usuario;
using static Netforemost.TechnicalTest.Common.Constants.Common;

namespace Netforemost.TechnicalTest.Seguridad.Api.Controllers
{
    [Authorize]
    [Authorize(AuthenticationSchemes = AuthenticateScheme.Seguridad)]
    [Route("[controller]")]
    [ApiController]
    public class SeguridadController : ControllerBase
    {
        private readonly ILogger<SeguridadController> _logger;
        private readonly Lazy<ILoginApplication> _loginApplication;

        public SeguridadController(ILogger<SeguridadController> logger, ILifetimeScope lifetimeScope)
        {
            _logger = logger;
            _loginApplication = new Lazy<ILoginApplication>(() => lifetimeScope.Resolve<ILoginApplication>());
        }
        private ILoginApplication LoginApplication => _loginApplication.Value;

        [AllowAnonymous]
        [HttpPost]
        [Route("Autenticar")]
        public async Task<IActionResult> Autenticar([FromBody] LoginModel model)
        {
            ResponseDTO response;
            try
            {
                if (model == null)
                    throw new FunctionalException(CommonResource.campo_requerido, "model = null");

                var servicioValidator = new LoginValidator().Validate(model);

                if (servicioValidator != null)
                {
                    if (!servicioValidator.IsValid)
                        throw new FunctionalException(CommonResource.campo_requerido, servicioValidator.ToString());
                }
                else
                    throw new FunctionalException(CommonResource.campo_requerido, "validator = null");
                response = await LoginApplication.Login(model);
            }
            catch (FunctionalException ex)
            {
                response = new ResponseDTO { Status = ex.Status, Message = ex.Message, Data = ex.Data, TransactionId = ex.TransactionId };
                _logger.LogWarning(ex.TransactionId, ex.Message, ex);
            }
            catch (TechnicalException ex)
            {
                response = new ResponseDTO { Status = ex.Status, Message = ex.Message, Data = ex.Data, TransactionId = ex.TransactionId };
                _logger.LogError(ex.TransactionId, ex.Message, ex);
            }
            catch (Exception ex)
            {
                response = new ResponseDTO { Status = EstadoRespuesta.ERROR_TECNICO, Message = ex.Message };
                _logger.LogError(response.TransactionId, ex.Message, ex);
            }
            return Ok(response);
        }

    }
}