using Autofac;
using Autofac.Core;
using Autofac.Extensions.DependencyInjection;
using Hangfire;
using Hangfire.Storage.SQLite;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Netforemost.TechnicalTest.Application.Implementations.Seguridad;
using Netforemost.TechnicalTest.Application.Interfaces.Seguridad;
using Netforemost.TechnicalTest.Common.Configurations;
using Netforemost.TechnicalTest.Common.Scheme;
using Netforemost.TechnicalTest.Extensions;
using Netforemost.TechnicalTest.Seguridad.Api.Hubs;
using System.Text.Json;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services
    .AddControllers()
    .AddXmlSerializerFormatters()
    .AddJsonOptions(options =>
        {
            options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
            options.JsonSerializerOptions.PropertyNamingPolicy = null;
        });
builder.Services.AddSignalR();
builder.Services.AddEndpointsApiExplorer();
var appSettingsSection = builder.Services.AddAppSettingExtension(builder.Configuration);
var appSettings = appSettingsSection.Get<AppSettings>();

builder.Services.AddJWTExtesion(builder.Configuration, AuthenticateScheme.Seguridad);
builder.Services.AddSwaggerExtension("Seguridad API");

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSwaggerGen(cfg =>
{
    cfg.CustomSchemaIds(type => type.ToString());
});


builder.Services.AddHangfire(config => config
        .UseSimpleAssemblyNameTypeSerializer()
        .UseRecommendedSerializerSettings()
        .UseSQLiteStorage(appSettings.DatabaseConfiguration.SqlLite.ConnectionStrings.DefaultConnection));

builder.Services.AddHangfireServer();


builder.Services.AddCors();
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(builder => builder.RegisterModule(new AutofacBusinessExtension()));

var app = builder.Build();

app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader()
);
app.UseHttpsRedirection();
app.UseRouting();
app.UseStaticFiles();

app.UseAuthorization();
app.UseEndpoints(endpoints =>
{
    endpoints.MapHub<UsuarioHub>("/usuario");
    endpoints.MapControllers();
});
app.UseHsts();
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");
    c.InjectStylesheet("/swagger/header.css");
});
app.UseHangfireDashboard();
app.MapHangfireDashboard();

RecurringJob.AddOrUpdate<IJobApplication>(x => x.EnvioCorreoBienvenidaUsuario(), "0 * * ? * *");

app.Run();
