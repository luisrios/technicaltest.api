﻿using FluentValidation;
using Netforemost.TechnicalTest.Model.Seguridad;

namespace Netforemost.TechnicalTest.Seguridad.Api.Validators.Seguridad
{
    public class LoginValidator : AbstractValidator<LoginModel>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Correo)
             .NotEmpty().WithMessage("El campo correo es obligatorio.")
             .EmailAddress().WithMessage("El formato del correo no es válido");

            RuleFor(x => x.Clave)
            .NotEmpty().WithMessage("El campo contraseña es obligatorio.");
        }
    }
}
