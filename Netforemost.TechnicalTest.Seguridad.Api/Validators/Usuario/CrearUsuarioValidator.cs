﻿using FluentValidation;
using Netforemost.TechnicalTest.Model.Seguridad;

namespace Netforemost.TechnicalTest.Seguridad.Api.Validators.Usuario
{
    public class CrearUsuarioValidator : AbstractValidator<CrearUsuarioModel>
    {
        public CrearUsuarioValidator()
        {

            RuleFor(c => c.Nombres).Cascade(CascadeMode.Stop)
                .NotNull().NotEmpty()
                .Configure(rule => rule.MessageBuilder = _ => "El campo nombres es requerido");

            RuleFor(c => c.Apellidos).Cascade(CascadeMode.Stop)
                .NotNull().NotEmpty()
                .Configure(rule => rule.MessageBuilder = _ => "El campo apellidos es requerido");

            RuleFor(c => c.Correo).Cascade(CascadeMode.Stop)
              .NotNull().NotEmpty()
              .Configure(rule => rule.MessageBuilder = _ => "El campo correo es requerido");

            RuleFor(x => x.Correo)
             .NotEmpty().WithMessage("El campo correo es obligatorio.")
             .EmailAddress().WithMessage("El formato del correo no es válido");

            RuleFor(c => c.Contrasena).Cascade(CascadeMode.Stop)
             .NotNull().NotEmpty()
             .Configure(rule => rule.MessageBuilder = _ => "El campo contraseña es requerido");

        }
    }
}
