﻿using Autofac;
using Microsoft.Extensions.Options;
using Netforemost.TechnicalTest.Application.Interfaces.Seguridad;
using Netforemost.TechnicalTest.Common.Configurations;
using Netforemost.TechnicalTest.Common.Exceptions;
using Netforemost.TechnicalTest.Domain.Models.Entities.Persona;
using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using Netforemost.TechnicalTest.Dto;
using Netforemost.TechnicalTest.Dto.Seguridad;
using Netforemost.TechnicalTest.Model.Seguridad;
using Netforemost.TechnicalTest.Repository.Interfaces.Data;
using Netforemost.TechnicalTest.Repository.Interfaces.Repositories.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Application.Implementations.Seguridad
{
    public class UsuarioApplication : IUsuarioApplication
    {
        private readonly Lazy<IUnitOfWork> _unitOfWork;
        private readonly AppSettings _settings;

        public UsuarioApplication(IOptions<AppSettings> appSettings, ILifetimeScope lifetimeScope)
        {
            _settings = appSettings.Value;
            _unitOfWork = new Lazy<IUnitOfWork>(() => lifetimeScope.Resolve<IUnitOfWork>());
        }
        private IUnitOfWork UnitOfWork => _unitOfWork.Value;
        private IUsuarioRepository UsuarioRepository => UnitOfWork.Repository<IUsuarioRepository>();

        public async Task<ResponseDTO> CrearUsuario(CrearUsuarioModel model)
        {
            var response = new ResponseDTO();

            var usuarioWhere = await UsuarioRepository.GetWhere(x => x.Usuario.ToLower() == model.Correo.Trim().ToLower());

            if (usuarioWhere != null && usuarioWhere.Any()) throw new FunctionalException("El correo del usuario ya está registrado.");

            var usuarioEntity = new UsuarioEntity()
            {
                Contrasena = model.Contrasena,
                Usuario = model.Correo,
                Persona = new PersonaEntity()
                {
                    Apellidos = model.Apellidos,
                    Correo = model.Correo,
                    Nombres = model.Nombres,
                }
            };

            await UnitOfWork.Set<UsuarioEntity>().AddAsync(usuarioEntity);
            await UnitOfWork.SaveChangesAsync();


            response.Data = new UsuarioDTO{ UsuarioId = usuarioEntity.Id, NombresApellidos = $"{model.Nombres} {model.Apellidos}" };


            return response;

        }
    }
}
