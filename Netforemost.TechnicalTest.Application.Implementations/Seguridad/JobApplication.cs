﻿using Autofac;
using Microsoft.Extensions.Options;
using Netforemost.TechnicalTest.Application.Interfaces.Seguridad;
using Netforemost.TechnicalTest.Common.Configurations;
using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using Netforemost.TechnicalTest.Repository.Interfaces.Data;
using Netforemost.TechnicalTest.Repository.Interfaces.Repositories.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Application.Implementations.Seguridad
{
    public class JobApplication : IJobApplication
    {
        private readonly Lazy<IUnitOfWork> _unitOfWork;
        private readonly AppSettings _settings;

        public JobApplication(IOptions<AppSettings> appSettings, ILifetimeScope lifetimeScope)
        {
            _settings = appSettings.Value;
            _unitOfWork = new Lazy<IUnitOfWork>(() => lifetimeScope.Resolve<IUnitOfWork>());
        }
        private IUnitOfWork UnitOfWork => _unitOfWork.Value;
        private IUsuarioRepository UsuarioRepository => UnitOfWork.Repository<IUsuarioRepository>();
        public async Task EnvioCorreoBienvenidaUsuario()
        {
            var listaUsuariosEntity = new List<UsuarioEntity>();
            var listaUsuariosUpdateEntity = new List<UsuarioEntity>();

            var usuarios = await UsuarioRepository.ObtenerUsuariosNuevos();
            if (usuarios != null && usuarios.Any())
            {
                listaUsuariosEntity = await UsuarioRepository.ObtenerUsuariosEntityPorIds(usuarios.Select(x => x.UsuarioId).ToList());
            }
            foreach (var item in listaUsuariosEntity)
            {
                var usuarioDTO = usuarios?.FirstOrDefault(x => x.UsuarioId == item.Id);

                if (usuarioDTO != null)
                {
                    //Logica de envio de correos
                    Console.WriteLine($"Enviando correo de invitacion al usuario {usuarioDTO.Nombres} {usuarioDTO.Apellidos} {DateTime.Now:yyyy-MM-dd HH:mm:ss}");

                    item.FlagEnvioCorreoBienvenida = true;
                    listaUsuariosUpdateEntity.Add(item);
                }
            }

            if (listaUsuariosUpdateEntity.Any())
            {
                UnitOfWork.Set<UsuarioEntity>().UpdateRange(listaUsuariosUpdateEntity);
                await UnitOfWork.SaveChangesAsync();
            }
        }
    }
}
