﻿using Autofac;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Netforemost.TechnicalTest.Application.Interfaces.Seguridad;
using Netforemost.TechnicalTest.Common;
using Netforemost.TechnicalTest.Common.Configurations;
using Netforemost.TechnicalTest.Common.Exceptions;
using Netforemost.TechnicalTest.Common.Resources;
using Netforemost.TechnicalTest.Dto;
using Netforemost.TechnicalTest.Dto.Seguridad;
using Netforemost.TechnicalTest.Model.Seguridad;
using Netforemost.TechnicalTest.Repository.Interfaces.Data;
using Netforemost.TechnicalTest.Repository.Interfaces.Repositories.Seguridad;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Runtime;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Application.Implementations.Seguridad
{
    public class LoginApplication : ILoginApplication
    {
        private readonly Lazy<IUnitOfWork> _unitOfWork;
        private readonly AppSettings _settings;

        public LoginApplication(IOptions<AppSettings> appSettings, ILifetimeScope lifetimeScope)
        {
            _settings = appSettings.Value;
            _unitOfWork = new Lazy<IUnitOfWork>(() => lifetimeScope.Resolve<IUnitOfWork>());
        }
        private IUnitOfWork UnitOfWork => _unitOfWork.Value;
        private IUsuarioRepository UsuarioRepository => UnitOfWork.Repository<IUsuarioRepository>();

        public async Task<ResponseDTO> Login(LoginModel model)
        {
            var response = new ResponseDTO();
            var loginResponse = new LoginDTO();

            var usuarioDTO = await UsuarioRepository.ObtenerUsuarioPorCorreoYClave(model);

            if (usuarioDTO == null) throw new FunctionalException(SeguridadResource.seguridad_login_credenciales_invalidos);

            var jwtConfig = _settings.JWTokenConfiguration;

            loginResponse.Usuario = usuarioDTO;
            loginResponse.Token = new TokenDTO
            {
                Value = GenerateToken(usuarioDTO.Codigo, usuarioDTO.Nombres, usuarioDTO.Apellidos, usuarioDTO.UsuarioId, usuarioDTO.Correo),
                FechaExpiracion = DateTime.UtcNow.AddHours(jwtConfig.ExpirationTimeHours)
            };

            response.Data = loginResponse;

            return response;
        }
        private string GenerateToken(string username, string nombres, string apellidos, int usuarioId, string email)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_settings.JWTokenConfiguration.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(Constants.Core.Token.CURRENT_USER, username.Trim().ToLower()),
                    new Claim(Constants.Core.Token.USER_ID, usuarioId.ToString().Trim()),
                    new Claim(Constants.Core.Token.NOMBRES, nombres ?? string.Empty),
                    new Claim(Constants.Core.Token.APELLIDOS, apellidos ?? string.Empty),
                    new Claim(Constants.Core.Token.CORREO, email ?? string.Empty ),
                }),
                Expires = DateTime.UtcNow.AddHours(_settings.JWTokenConfiguration.ExpirationTimeHours),
                Audience = _settings.JWTokenConfiguration.Aud,
                Issuer = _settings.JWTokenConfiguration.Iss,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

    }
}
