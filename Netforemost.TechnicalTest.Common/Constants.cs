﻿namespace Netforemost.TechnicalTest.Common
{
    public static class Constants
    {
        public struct Core
        {
            public struct HttpStatus
            {
                public const int Mantenimiento = 800;
                public const int Ok = 200;
            }

            public struct Audit
            {
                public const string CREATION_USER = "CreationUser";
                public const string CREATION_DATE = "CreationDate";
                public const string MODIFICATION_USER = "ModificationUser";
                public const string MODIFICATION_DATE = "ModificationDate";
                public const string ROW_STATUS = "RowStatus";
                public const string SYSTEM = "System";
            }
            public struct UserClaims
            {
                public const string UserId = "UserId";
                public const string UserName = "UserName";
                public const string Role = "Role";
                public const string FullName = "FullName";
                public const string Society = "Sociedad";
                public const string ServiceOrganization = "ServiceOrganization";
            }
            public struct DateTimeFormats
            {
                public const string DD_MM_YYYY_HH_MM_SS_FFF = "yyyyMMddHHmmssFFF";

                public const string DD_MM_YYYY_HH_MM_SS_TT = "dd/MM/yyyy hh:mm:ss tt";
                public const string DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy hh:mm:ss";
                public const string DD_MM_YYYY = "dd/MM/yyyy";
                public const string DD_MM_YYYY_ConPunto = "dd.MM.yyyy";

                public const string D_MM_YYYY = "d/MM/yyyy";
                public const string D_MM_YYYY_HH_MM_SS = "d/MM/yyyy hh:mm:ss";
                public const string D_MM_YYYY_HH_MM_SS_TT = "d/MM/yyyy hh:mm:ss tt";

                public const string MM_DD_YYYY = "MM/dd/yyyy";
                public const string MM_DD_YYYY_HH_MM_SS_TT = "MM/dd/yyyy  hh:mm:ss tt";
                public const string MM_DD_YYYY_HH_MM_SS = "MM/dd/yyyy  hh:mm:ss";

                public const string MM_D_YYYY = "MM/d/yyyy";
                public const string MM_D_YYYY_HH_MM_SS_TT = "MM/d/yyyy hh:mm:ss tt";
                public const string MM_D_YYYY_HH_MM_SS = "MM/d/yyyy hh:mm:ss";
                public const string YYYY_MM = "yyyy-MM";
                public const string HH_MM = "hh:mm tt";
            }
            public struct DateTimeReplace
            {
                public const string HH_MM_SS = " 00:00:00";
            }
            public static class Token
            {
                public const string CURRENT_USER = "CurrentUser";
                public const string USER_ID = "UserId";
                public const string NOMBRES = "Nombres";
                public const string APELLIDOS = "Apellidos";
                public const string CORREO = "Email";
            }
            public const int CantidadMesesAtras = -11;
            public const string CultureInfoPeru = "es-PE";
        }
        public static class Seguridad
        {
            public struct TipoUsuario
            {
                public const string CLIENTE = "C";
                public const string ASESOR = "A";
            }

            public struct RolInterno
            {
                public const string ADMINISTRADOR_AEL = "00000001";
                public const string ADMINISTRADOR_COSMOS = "00000002";
                public const string ADMINISTRADOR_AHORROS = "00000007";
                public const string INGENIERO_DE_SERVICIO = "00000003";
                public const string REPRESENTANTE_DE_VENTAS = "00000004";
                public const string ASESOR_DE_SOLICITUDES = "00000005";
                public const string ADMINISTRADOR_DE_SOLICITUDES = "00000006";
                public const string ADMINISTRADOR_DE_UNIDAD_NEGOCIO_ENERGIA = "00000008";
                public const string JEFE_SERVICIO = "00000009";
                public const string ADMINISTRADOR_PRODUCTIVIDAD = "00000010";
                public const string REPRESENTANTE_DE_CVA = "00000011";
                public const string ADMINISTRADOR_CVA = "00000012";
            }
            public struct MenuCategoriasPortal
            {
                public const string INDICADORES = "CATMIIN";
            }
            public struct LlaveEncriptacion
            {
                public const string APP = "493d6911-e57b-47de-9239-cd66247f3921";
                public const int LONGITUD_SALT = 10;
            }
            public struct Opcion
            {
                public const string MIS_EQUIPOS = "FLOMEQ";
            }
        }
        public struct Common
        {
            public struct EstadoRespuesta
            {
                public const int ERROR_FUNCIONAL = 1;
                public const int OK = 0;
                public const int ERROR_TECNICO = -1;
                public const int MANTENAINCE = 8;
            }
        }
    }
}