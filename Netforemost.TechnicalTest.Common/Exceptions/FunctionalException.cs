﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Netforemost.TechnicalTest.Common.Constants.Common;

namespace Netforemost.TechnicalTest.Common.Exceptions
{
    [Serializable()]
    public class FunctionalException : NetforemostException
    {
        public FunctionalException(string message) : base(EstadoRespuesta.ERROR_FUNCIONAL, message)
        {
        }

        public FunctionalException(string message, dynamic data) : base(EstadoRespuesta.ERROR_FUNCIONAL, message)
        {
            Data = data;
        }

        public FunctionalException(int estado, string message, dynamic data) : base(estado, message)
        {
            Data = data;
        }
    }
}
