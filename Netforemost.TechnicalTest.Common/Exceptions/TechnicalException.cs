﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Netforemost.TechnicalTest.Common.Constants.Common;

namespace Netforemost.TechnicalTest.Common.Exceptions
{
    [Serializable()]
    public class TechnicalException : NetforemostException
    {
        public TechnicalException(string message) : base(EstadoRespuesta.ERROR_TECNICO, message)
        {
        }

        public TechnicalException(string message, dynamic data) : base(EstadoRespuesta.ERROR_TECNICO, message)
        {
            Data = data;
        }
    }
}
