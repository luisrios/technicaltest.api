﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Common.Exceptions
{
    public class NetforemostException: Exception, ISerializable
    {
        public string TransactionId { get; }
        public int Status { get; }
        public dynamic Data { get; set; }

        public NetforemostException(int statusCode, string message) : base(message)
        {
            Status = statusCode;
            TransactionId = DateTime.Now.ToString(Constants.Core.DateTimeFormats.DD_MM_YYYY_HH_MM_SS_FFF);
        }
    }
}
