﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Common.Configurations
{
    public class DatabaseConfiguration
    {
        public SqlServer SqlServer { get; set; }
        public SqlLite SqlLite { get; set; }
    }

    public partial class SqlServer
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }

    public partial class SqlLite
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }

    public partial class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }
}
