﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Common.Configurations
{
    public class AppSettings
    {
        public DatabaseConfiguration DatabaseConfiguration { get; set; }
        public JwTokenConfiguration JWTokenConfiguration { get; set; }
    }
}
