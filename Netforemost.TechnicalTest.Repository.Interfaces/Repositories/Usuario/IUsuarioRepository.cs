﻿using Netforemost.TechnicalTest.Domain.Models.Entities.Usuario;
using Netforemost.TechnicalTest.Dto.Seguridad;
using Netforemost.TechnicalTest.Model.Seguridad;
using Netforemost.TechnicalTest.Repository.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Interfaces.Repositories.Seguridad
{
    public interface IUsuarioRepository : IBaseRepository<UsuarioEntity>
    {
        Task<UsuarioDTO> ObtenerUsuarioPorCorreoYClave(LoginModel model);
        Task<List<UsuarioDTO>> ObtenerUsuariosNuevos();
        Task<List<UsuarioEntity>> ObtenerUsuariosEntityPorIds(List<int> ids);
    }
}
