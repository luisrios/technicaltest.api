﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netforemost.TechnicalTest.Repository.Interfaces.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void SaveChanges();
        Task SaveChangesAsync();

        bool HasChanges();

        void Dispose(bool disposing);

        T Repository<T>() where T : class;

        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        DbContext Get();
    }
}
